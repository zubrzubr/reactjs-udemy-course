import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';
import Char from './Char/Char';

class App extends Component {
  state = {
    message: ['Default', 'Message'],
  }
  changeMessageHandler = (event) => {
    const state = {...this.state};
    state.message = event.target.value.split(' ');
    this.setState(state);
  };
  removeLetterHandler = (letter) => {
    const state = {...this.state};
    const replacedMessage = state.message.join().replace(letter, '');
    state.message = replacedMessage.split(',');
    this.setState(state);
  }
  getMessageLetters = () => {
    return this.state.message.join('').split('');
  }
  render() {
    let messageLetters = this.getMessageLetters();
    return (
      <div className="App">
        <UserInput change={(event) => this.changeMessageHandler(event)} message={this.state.message}></UserInput>
        <UserOutput message={this.state.message}></UserOutput>
        {messageLetters.map((letter, index) => {
          return <Char letter={letter} key={index} click={() => this.removeLetterHandler(letter)}></Char>;
        })}
      </div>
    );
  }
}

export default App;
