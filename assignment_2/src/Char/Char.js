import React, { Component } from 'react';
import './Char.css';

class Char extends Component {
  render() {
    return (
      <div className="CharWrapper" onClick={this.props.click}>
        {this.props.letter}
      </div>
    );
  }
}

export default Char;
