import React, { Component } from 'react';

class Validation extends Component {
  isLengthCorrect = (messageLength) => {
    if (messageLength > this.props.allowedLength) {
      return true;
		}
		return false;
  }
  render() {
    let outputMessage = '';
    const style = {
      color: '#9b1212',
    };
    if (this.isLengthCorrect(this.props.messageLength)) {
			outputMessage = 'Text long enough'
      style.color = '#76db62';
    } else {
			outputMessage = 'Text too short'
		};

    return (
      <div className="ValidationWrapper">
        <p style={style}>{outputMessage}</p>
      </div>
    );
  }
}

export default Validation;
