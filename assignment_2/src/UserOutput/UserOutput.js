import React, { Component } from 'react';
import './UserOutput.css';
import Validation from '../Validation/Validation';

class UserOutput extends Component {
  getMessageLength = (messageList) => {
    let messageLength = 0;
    for (let m of messageList) {
      messageLength += m.length;
    }
    return messageLength;
  };

  render() {
    const messageLength = this.getMessageLength(this.props.message);
    return (
      <div>
        <p className='MessageCount'>{messageLength}</p>
        <Validation messageLength={messageLength} allowedLength='8'></Validation>
      </div>
    );
  }
}

export default UserOutput;
