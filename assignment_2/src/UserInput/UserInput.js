import React, { Component } from 'react';
import './UserInput.css';

class UserInput extends Component {
  displayMessage = () => {
    return this.props.message.join(' ');
  }
  render() {
    const message = this.displayMessage();
    return (
      <div>
            <input className='UserInput' onChange={this.props.change} type="text" value={message}/>
      </div>
    );
  }
}

export default UserInput;
