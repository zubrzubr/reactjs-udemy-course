import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';


class App extends Component {
  state = {
    persons: [
      {id: 1, name: 'Max', age: 26},
      {id: 2, name: 'Bob', age: 32},
      {id: 3, name: 'Jack', age: 22},
    ],
    showPersons: false,
  }
  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }
  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    }
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({persons: persons});
  }
  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow})
  }
  render() {
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid black',
      padding: '8px',
      cursor: 'pointer'
    };
    let persons = null;
    let buttonText = 'Show Persons';
    if (this.state.showPersons) {
      buttonText =  'Hide Persons';
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
            return <Person 
              name={person.name} 
              age={person.age} 
              click={() => this.deletePersonHandler(index)}
              changed={(event) => this.nameChangeHandler(event, person.id)}
              key={person.id}>Person id: {person.id}</Person>
          })}
        </div>
      );
      style.backgroundColor = 'red';
    }
    return (
      <div className="App">
        <h1>First react Component</h1> 
        <p>This is really working</p>
        <button
          style={style} 
          onClick={() => this.togglePersonsHandler()}>{buttonText}</button>
      {persons}
      </div>
    );
  }
}

export default App;
