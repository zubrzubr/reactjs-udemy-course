import React, { Component } from 'react';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';
import './App.css';

class App extends Component {
  state = {
    teams: [
      {name: 'Milan'},
      {name: 'Inter'},
    ]
  }
  TeamNameHandler = (nameReplace) => {
    this.setState({teams: [
      {name: nameReplace},
      {name: nameReplace},
    ]})
  }
  TeamNameChangeHandler = (event) => {
    this.setState({teams: [
      {name: event.target.value},
      {name: event.target.value},
    ]})
  }
  render() {
    return (
      <div className="App">
        <h1>Who will win?!</h1>
        <UserInput change={this.TeamNameChangeHandler} name={this.state.teams[0].name}></UserInput>
        <UserOutput click={this.TeamNameHandler.bind(this, 'MILAN')} name={this.state.teams[0].name}>MILAN: Because we are stronger!</UserOutput>
        <UserOutput click={this.TeamNameHandler.bind(this, 'INTER')} name={this.state.teams[1].name}>INTER: Because we are faster!</UserOutput>
      </div>
    );
  }
}

export default App;
