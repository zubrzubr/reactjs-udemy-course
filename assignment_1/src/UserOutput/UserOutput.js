import React from 'react';

const userOutput = (props) => {
    const style = {
        backgroundColor: '#4AB7B5',
        font: 'ingerit',
        border: '2px solid #4EABAC',
        padding: '8px',
        cursor: 'pointer',
        color: '#3A5870'
      };
    return (
        <div>
            <p>{props.children}</p>
            <p style={style} onClick={props.click}>{props.name}</p>
        </div>
    )
}

export default userOutput;
